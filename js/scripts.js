(function ($) {
	'use strict';

	// PATIENTS CAROUSEL
	$(".patients-carousel, .medical-carousel").owlCarousel({
		mouseDrag: true,
		margin: 25,
		touchDrag: true,
		navText: [
			"<i class='fa fa-angle-left fa-2x'></i>",
        	"<i class='fa fa-angle-right fa-2x'></i>"
		],
		nav: true,
		responsiveClass: true,
		responsive: {
			0:{
				items: 1
			},
			570:{
				items: 2
			},
			600:{
				items: 3
			},
			1000:{
				items: 4
			},
		}
	});

	// CLONE SEARCH-BUTTON
	$(".box-search-container").clone(false).appendTo($(".rwd-box-search"));

	// CLONE NAVIGATION TO RWD-NAVIGATION-SIDEBAR
	$(".main-navigation > ul").clone(false).find("ul, li").removeAttr("id").remove(".submenu").appendTo($(".nav-rwd-sidebar > ul"));

	// TOGGLE RWD-NAVIGATION-SIDEBAR
	$(".btn-show, .btn-hide").click( function(e) {
		var nav_side = $("#nav-rwd-content");
		var wrapper = $(".wrapper-inner");
		nav_side.toggleClass("rwd-sidebar-active");
		wrapper.toggleClass("on-active");
	});

	new WOW().init();

	// TOGGLE SEARCH-FORM
	$(".box-search-container .box-search").click( function() {
		event.preventDefault();
		$(this).find('i').toggleClass( 'fa-times', 
			'fa-search' );
		var box = $(".form-search");
		box.slideToggle(250);
	});

})(jQuery);